﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace consolaVS_AngelOliveros
{
    class Program
    {
        static void GenerarMenu()
        {
            Console.WriteLine("*******************");
            Console.WriteLine("1 - Web valida");
            Console.WriteLine("2 - Contar hasta");
            Console.WriteLine("3 - Cadena inversa");
            Console.WriteLine("4 - Hola mundo");
            Console.WriteLine("5 - Salir");
            Console.WriteLine("*******************");
        }
        static void ValidarWeb()
        {
            Console.WriteLine("Escriba url");
            string aux = Console.ReadLine();
            if(System.Text.RegularExpressions.Regex.IsMatch(aux, "^www\\.[^\\s]*\\.(net|com)$"))
            {
                Console.WriteLine("Es valida");
            } else
            {
                Console.WriteLine("No es valida");
            }

        }

        static void Contar()
        {
            Console.WriteLine("Escriba un numero entero positivo");
            int aux = int.Parse(Console.ReadLine());
            for(int i = 0; i <= aux; i++)
            {
                Console.WriteLine(i);
            }
        }

        static void CadenaInversa()
        {
            Console.WriteLine("Escriba cadena");
            string aux = Console.ReadLine();
            Console.WriteLine("La cadena inversa es:");
            for(int i = aux.Length-1; i >= 0; i--)
            {
                Console.Write(aux[i]);
            }
            Console.WriteLine();
        }
        static void Saludar()
        {
            Console.WriteLine("Como te llamas?");
            string aux = Console.ReadLine();
            Console.WriteLine("Hola " + aux + "!");
        }
        static void Main(string[] args)
        {
            bool seguir = true;
            string resp;
            do
            {
                GenerarMenu();
                resp = Console.ReadLine();
                switch (resp)
                {
                    case "1":
                        ValidarWeb();
                        break;
                    case "2":
                        Contar();
                        break;
                    case "3":
                        CadenaInversa();
                        break;
                    case "4":
                        Saludar();
                        break;
                    case "5":
                        Console.WriteLine("Saliendo");
                        seguir = false;
                        break;
                    default:
                        Console.WriteLine("Comando desconocido");
                        break;
                }
            } while (seguir);
            Console.WriteLine("Pulse alguna tecla ...");
            Console.ReadKey();
        }
    }
}
