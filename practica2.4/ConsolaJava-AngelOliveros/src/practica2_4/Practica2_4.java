package practica2_4;

import java.util.Scanner;

public class Practica2_4 {
	//pre:sc debe estar inicializado a
		//	la salida estandar
		//post: pide por entrada estandar una
		//	url e indica si es del formato
		//	espepcificado en el enunciado
		public static void validarWeb(Scanner sc) {
			String url;
			System.out.print("Escriba la url: ");
			url=sc.nextLine();
			//adicionalmente, no acepta cualquier caracter
			//separador (tabulado, final de linea, etc...)
			if(url.matches("^www\\.[^\\s]*\\.(net|com)$")){
				System.out.println("web correcta");
			}
			else {
				System.out.println("web incorrecta");
			}
			
		}
		
		//pre:sc debe estar inicializado a
		//	la salida estandar
		//post: pide por entrada estandar tres
		//	numeros enteros y los escribe
		//	por pantalla organizados en orden
		//	decreciente
		public static void numsDecrecientes(Scanner sc) {

			System.out.print("Escriba tres enteros: ");
			int mayor, medio, menor, aux;
			mayor=sc.nextInt();
			
			aux=sc.nextInt();
			if(aux>mayor) {
				medio=mayor;
				mayor=aux;
			}
			else {
				medio=aux;
			}
			
			aux=sc.nextInt();
			if(aux>mayor) {
				menor=medio;
				medio=mayor;
				mayor=aux;
			}
			else if(aux>medio) {
				menor=medio;
				medio=aux;
			}
			else {
				menor=aux;
			}
			System.out.println(menor + "<" + medio + "<" + mayor);
			//vaciado de buffer para evitar lecturas
			//no deseadas posteriormente
			sc.nextLine();
			
		}
		
		//pre:sc debe estar inicializado a
		//	la salida estandar
		//post: pide por entrada estandar los 
		//	nombres de tres personas (nombre y apellidos)
		//	y los escribe por pantalla en minusculas
		//	y en orden alfabetico segun apellido
		public static void ordenaNombres(Scanner sc) {
			String nombre1, apellidos1,
					nombre2, apellidos2,
					nombre3, apellidos3;
			
			System.out.println("Primera persona:");
			System.out.println("escriba los apellidos");
			apellidos1=sc.nextLine();
			System.out.println("escriba nombre");
			nombre1=sc.nextLine();
			nombre1=apellidos1 + ", " +  nombre1;
			
			System.out.println("Segunda persona:");
			System.out.println("escriba los apellidos");
			apellidos2=sc.nextLine();
			System.out.println("escriba nombre");
			nombre2=sc.nextLine();
			nombre2=apellidos2 + ", " +  nombre2;
			
			System.out.println("Tercera persona:");
			System.out.println("escriba los apellidos");
			apellidos3=sc.nextLine();
			System.out.println("escriba nombre");
			nombre3=sc.nextLine();
			nombre3=apellidos3+ ", " +  nombre3;
			
			if(nombre1.compareToIgnoreCase(nombre2)<0) {
				//n1 primero o segundo
				
				if(nombre1.compareToIgnoreCase(nombre3)<0) {
					//n1 primero, n2 segundo o tercero
					
					if(nombre2.compareToIgnoreCase(nombre3)<0) {
						//n1 n2 n3
						System.out.println(nombre1);
						System.out.println(nombre2);
						System.out.println(nombre3);
					}
					else {
						//n1 n3 n2
						System.out.println(nombre1);
						System.out.println(nombre3);
						System.out.println(nombre2);
					}
				}
				else {
					//n3 n1 n2
					System.out.println(nombre3);
					System.out.println(nombre1);
					System.out.println(nombre2);
				}
			}
			else {
				//n2 primero o segundo
				
				if(nombre2.compareToIgnoreCase(nombre3)<0) {
					//n2 primero, n1 segundo o tercero
					
					if(nombre1.compareToIgnoreCase(nombre3)<0) {
						//n2 n1 n3
						System.out.println(nombre2);
						System.out.println(nombre1);
						System.out.println(nombre3);
					}
					else {
						//n2 n3 n1
						System.out.println(nombre2);
						System.out.println(nombre3);
						System.out.println(nombre1);
					}
				}
				else {
					//n3 n2 n1
					System.out.println(nombre3);
					System.out.println(nombre2);
					System.out.println(nombre1);
				}
			}
		}
		
		public static void cadenaInversa(Scanner sc) {
			System.out.println("Escriba una cadena");
			String aux = sc.nextLine();
			System.out.println("La cadena inversa es");
			for(int i = aux.length()-1; i>=0; i--) {
				System.out.print(aux.charAt(i));
			}
			System.out.println();
		}
		//pre:sc debe estar inicializado a
		//	la salida estandar
		//post: genera un menu con cuatro 
		//	pociones segun el enunciado e
		//	interpreta las respuestas del
		//	usuario hasta que indique salir
		//	(escribiendo 4 por entrada
		//	estandar)
		public static void generarMenu(Scanner sc) {
			boolean seguir=true;
			String input;
			 do{
				System.out.println("******************************");
				System.out.println("1 - Validar Web");
				System.out.println("2 - Números de mayor a menor");
				System.out.println("3 – Ordenar personas");
				System.out.println("4 - Cadena inversa");
				System.out.println("5 - Salir");
				System.out.println("******************************");
				System.out.print("Introduzca el número de opción: ");
			
				input=sc.nextLine();
			
				switch(input) {
					case "1":
						System.out.println("Escogido: Validar Web");
						validarWeb(sc);
						break;
					case "2":
						System.out.println("Escogido: Números de mayor a menor");
						numsDecrecientes(sc);
						break;
					case "3":
						System.out.println("Escogido: Ordenar personas");
						ordenaNombres(sc);
						break;
					case "4":
						System.out.println("Escogido: Cadena inversa");
						cadenaInversa(sc);
						break;
					case "5":
						System.out.println("Finalizando");
						seguir=false;
						break;
					default:
						System.out.println("Comando desconocido");
				}
			}while(seguir);
		}
		
		public static void main(String args[]){
			Scanner sc = new Scanner(System.in);
			generarMenu(sc);
			sc.close();
		}

}
