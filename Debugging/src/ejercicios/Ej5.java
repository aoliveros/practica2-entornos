package ejercicios;

import java.util.Scanner;

public class Ej5 {

	public static void main(String[] args) {
		/*
		 * Ayudate del debugger para entender qué realiza este programa
		 */
		
		
		Scanner lector;
		int numeroLeido;
		int cantidadDivisores = 0;
		
		lector = new Scanner(System.in);
		System.out.println("Introduce un numero");
		numeroLeido = lector.nextInt();
		//Pide y lee un numero de entrada estandar
		
		for (int i = 1; i <= numeroLeido; i++) {
			//repite para todos los enteros
			//entre 1 y mi numero
			if(numeroLeido % i == 0){
				cantidadDivisores++;
				//si mi numero es multiplo de i
				//suma 1 a cantidadDivisores
			}
		}
		
		if(cantidadDivisores > 2){
			//si mi numero tiene mas de dos divisores
			System.out.println("No lo es");
			//escribe "No lo es" por consola
		}else{
			//si el numero tiene dos divisores
			//(1 y el mismo); es decir, si el
			//numero es primo
			System.out.println("Si lo es");
			//escribe "Si lo es" por consola
		}
		
		
		lector.close();
	}

}
