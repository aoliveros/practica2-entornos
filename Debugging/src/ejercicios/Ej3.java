package ejercicios;

import java.util.Scanner;

public class Ej3 {

	public static void main(String[] args) {
		/*Establecer un breakpoint en la primera instrucción y avanzar
		 * instrucción a instrucción (step into) analizando el contenido de las variables
		 */
		
		Scanner lector;
		char caracter=27;
		int posicionCaracter;
		String cadenaLeida, cadenaFinal;
		
		lector = new Scanner(System.in);
		System.out.println("Introduce un cadena");
		cadenaLeida = lector.nextLine();
		
		posicionCaracter = cadenaLeida.indexOf(caracter);
		if(posicionCaracter>=0) {//correccion
			/*Error*/cadenaFinal = cadenaLeida.substring(0, posicionCaracter);
			//Se lanza una excepcionr al intentar devolver un string entre los
			//indices [0,-1](fuera de rango. El -1 aparece porque no consigue
			//encontrar en el string ningun caracter con el ASCII 27
			//solucion: verficar que se hay encontrado el caracter antes de
			//tratar de obtener el substring
			//if((posicionCaracter = cadenaLeida.indexOf(caracter))<0){
				//cadenaFinal = cadenaLeida.substring(0, posicionCaracter);
			//}
			System.out.println(cadenaFinal);
		}
		
		lector.close();
	}

}
