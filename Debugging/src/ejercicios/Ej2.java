package ejercicios;

import java.util.Scanner;

public class Ej2 {
	public static void main(String[] args){
		/*Establecer un breakpoint en la primera instrucción y avanzar
		 * instrucción a instrucción (step into) analizando el contenido de las variables
		 */
		
		Scanner input ;
		int numeroLeido;
		String cadenaLeida;		
		
		input = new Scanner(System.in);
		
		System.out.println("Introduce un numero ");
		numeroLeido = input.nextInt();
		
		input.nextLine();//correccion
		
		System.out.println("Introduce un numero como String");
		cadenaLeida = input.nextLine();
		
/*ERROR*/if ( numeroLeido == Integer.parseInt(cadenaLeida)){//error al ejecutar parseInt de
		//una cadena vacia
		//cadenaLeida contiene una cadena vacia porque el buffer no ha sido limpiado tras 
		//la lectura de un entero
		//solucion: limpiar el buffer del scanner tras la lectura del entero
		//input.nextLine();
			System.out.println("Lo datos introducidos reprensentan el mismo número");
		}
		
		input.close();
		
		
		
	}
}
