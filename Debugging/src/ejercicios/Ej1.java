package ejercicios;

import java.util.Scanner;

public class Ej1 {

	public static void main(String[] args) {
		/*Establecer un breakpoint en la primera instrucción y avanzar
		 * instrucción a instrucción (step into) analizando el contenido de las variables
		 *  
		 */
		
		
		
		Scanner input;
		int cantidadVocales, cantidadCifras;
		String cadenaLeida;
		char caracter;
		
		cantidadVocales = 0;
		cantidadCifras = 0;
		
		System.out.println("Introduce una cadena de texto");
		input = new Scanner(System.in);
		cadenaLeida = input.nextLine();
		
		for(int i = 0 ; i < cadenaLeida.length(); i++){//corregida
			
		//for(int i = 0 ; i <= cadenaLeida.length(); i++){
			
/*ERROR*/	caracter = cadenaLeida.charAt(i);//lanza excepción para i==cadenaLida.length()
			//porque intenta acceder a un caracter fuera del rango del string.
			//solución: reducir en 1 iteración el bucle para
			//que no exceda el rango de indices del string
			//for(int i = 0 ; i < cadenaLeida.length(); i++)
			if(caracter == 'a' || caracter == 'e' || caracter == 'i' 
					|| caracter == 'o' || caracter == 'u'){
					cantidadVocales++;
			}
			if(caracter >= '0' && caracter <='9'){
				cantidadCifras++;
			}
			
		}
		System.out.println(cantidadVocales + " y " + cantidadCifras);
		
		
		input.close();
	}

}
