package gui;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import java.awt.Toolkit;
import javax.swing.JMenuBar;
import javax.swing.JMenu;
import javax.swing.JMenuItem;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.JTextField;
import javax.swing.JCheckBox;
import javax.swing.JSlider;
import javax.swing.JLabel;
import javax.swing.JSpinner;
import javax.swing.JEditorPane;
import javax.swing.JRadioButton;
import javax.swing.JComboBox;
import javax.swing.JScrollBar;
import javax.swing.JTextArea;
import javax.swing.JScrollPane;
import javax.swing.JFormattedTextField;
import javax.swing.JProgressBar;
import java.awt.ScrollPane;
import javax.swing.JTextPane;
import javax.swing.JToggleButton;
import javax.swing.JToolBar;
import javax.swing.JTabbedPane;
import javax.swing.JList;
import javax.swing.DefaultComboBoxModel;

public class Ventana extends JFrame {

	private JPanel contentPane;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Ventana frame = new Ventana();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * @author Angel Oliveros
	 * @since 2018-01-24
	 */
	public Ventana() {
		setTitle("VentanaEclipse");
		setIconImage(Toolkit.getDefaultToolkit().getImage("C:\\Users\\loide\\Documents\\Daw\\1ero\\Entornos\\practica2.3\\src\\gui\\imagenes\\icons8-advertisement-page-50.png"));
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 606, 404);
		
		JMenuBar menuBar = new JMenuBar();
		setJMenuBar(menuBar);
		
		JMenu mnFile = new JMenu("File");
		menuBar.add(mnFile);
		
		JMenuItem mntmMon = new JMenuItem("mon");
		mnFile.add(mntmMon);
		
		JMenuItem mntmMenu = new JMenuItem("menu2");
		mnFile.add(mntmMenu);
		
		JMenuItem mntmMenu_1 = new JMenuItem("menu3");
		mnFile.add(mntmMenu_1);
		
		JMenu mnMenu = new JMenu("menu4");
		mnFile.add(mnMenu);
		
		JMenuItem mntmItem = new JMenuItem("item1");
		mnMenu.add(mntmItem);
		
		JMenuItem mntmItem_1 = new JMenuItem("item2");
		mnMenu.add(mntmItem_1);
		
		JMenu mnEdit = new JMenu("Edit");
		menuBar.add(mnEdit);
		
		JMenuItem mntmCopy = new JMenuItem("copy");
		mnEdit.add(mntmCopy);
		
		JMenuItem mntmPaste = new JMenuItem("paste");
		mnEdit.add(mntmPaste);
		
		JMenu mnOther = new JMenu("other");
		mnEdit.add(mnOther);
		
		JMenu mnSource = new JMenu("Source");
		menuBar.add(mnSource);
		
		JMenuItem mntmOpcion = new JMenuItem("opcion1");
		mnSource.add(mntmOpcion);
		
		JMenuItem mntmOpcion_1 = new JMenuItem("opcion2");
		mnSource.add(mntmOpcion_1);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JButton btnDoIt = new JButton("do it");
		btnDoIt.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
			}
		});
		btnDoIt.setBounds(0, 48, 89, 23);
		contentPane.add(btnDoIt);
		
		JCheckBox chckbxHePulsado = new JCheckBox("he pulsado");
		chckbxHePulsado.setBounds(10, 78, 97, 23);
		contentPane.add(chckbxHePulsado);
		
		JSlider slider = new JSlider();
		slider.setPaintLabels(true);
		slider.setPaintTicks(true);
		slider.setBounds(10, 123, 200, 26);
		contentPane.add(slider);
		
		JLabel lblGradoDeSatisfaccion = new JLabel("Grado de satisfaccion");
		lblGradoDeSatisfaccion.setBounds(40, 160, 147, 14);
		contentPane.add(lblGradoDeSatisfaccion);
		
		JLabel lblPulsaElBoton = new JLabel("Pulsa el boton");
		lblPulsaElBoton.setBounds(99, 52, 111, 14);
		contentPane.add(lblPulsaElBoton);
		
		JSpinner spinner = new JSpinner();
		spinner.setBounds(551, 11, 29, 20);
		contentPane.add(spinner);
		
		JLabel lblSpinner = new JLabel("spinner");
		lblSpinner.setBounds(495, 14, 46, 14);
		contentPane.add(lblSpinner);
		
		JRadioButton rdbtnSoyZurdo = new JRadioButton("Soy zurdo");
		rdbtnSoyZurdo.setBounds(254, 48, 109, 23);
		contentPane.add(rdbtnSoyZurdo);
		
		JRadioButton rdbtnSoyDiestro = new JRadioButton("Soy diestro");
		rdbtnSoyDiestro.setBounds(254, 78, 109, 23);
		contentPane.add(rdbtnSoyDiestro);
		
		JFormattedTextField formattedTextField = new JFormattedTextField();
		formattedTextField.setBounds(221, 283, -152, 14);
		contentPane.add(formattedTextField);
		
		JProgressBar progressBar = new JProgressBar();
		progressBar.setIndeterminate(true);
		progressBar.setBounds(10, 185, 146, 14);
		contentPane.add(progressBar);
		
		JLabel lblPulsarOverload = new JLabel("Pulsar Overload");
		lblPulsarOverload.setBounds(164, 185, 97, 14);
		contentPane.add(lblPulsarOverload);
		
		JTextPane txtpnEscribaAqui = new JTextPane();
		txtpnEscribaAqui.setText("Escriba aqui");
		txtpnEscribaAqui.setBounds(55, 222, 97, 100);
		contentPane.add(txtpnEscribaAqui);
		
		JToggleButton tglbtnToggle = new JToggleButton("toggle");
		tglbtnToggle.setBounds(459, 42, 121, 23);
		contentPane.add(tglbtnToggle);
		
		JToolBar toolBar = new JToolBar();
		toolBar.setBounds(2, 0, 270, 26);
		contentPane.add(toolBar);
		
		JButton btnNewButton = new JButton("boton1");
		toolBar.add(btnNewButton);
		
		JButton btnBoton = new JButton("boton2");
		toolBar.add(btnBoton);
		
		JButton btnBoton_1 = new JButton("boton3");
		toolBar.add(btnBoton_1);
		
		JButton btnBoton_2 = new JButton("boton4");
		toolBar.add(btnBoton_2);
		
		JButton btnBoton_3 = new JButton("boton5");
		toolBar.add(btnBoton_3);
		
		JTabbedPane tabbedPane = new JTabbedPane(JTabbedPane.TOP);
		tabbedPane.setBounds(276, 134, 314, 210);
		contentPane.add(tabbedPane);
		
		JPanel panel_2 = new JPanel();
		tabbedPane.addTab("Pestaña1", null, panel_2, null);
		panel_2.setLayout(null);
		
		JList list = new JList();
		list.setBounds(154, 5, 0, 0);
		panel_2.add(list);
		
		JComboBox comboBox = new JComboBox();
		comboBox.setModel(new DefaultComboBoxModel(new String[] {"Hola", "Adios", "????", "Delete this"}));
		comboBox.setBounds(64, 36, 63, 29);
		panel_2.add(comboBox);
		
		JPanel panel = new JPanel();
		tabbedPane.addTab("Pestaña2", null, panel, null);
		panel.setLayout(null);
		
		JButton btnDfh = new JButton("dfh");
		btnDfh.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
			}
		});
		btnDfh.setBounds(58, 11, 89, 23);
		panel.add(btnDfh);
		
	}
}
