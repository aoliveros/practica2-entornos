package paquete;

import source.Cifras;
import source.Letras;

public class PruebasLibreria {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		String cadena = "Hello World";
		System.out.println(cadena);
		System.out.println(Letras.toLowerCase(cadena));
		System.out.println(Letras.toUpperCase(cadena));
		System.out.println(Letras.startsWith(cadena));
		int a = 900;
		int b = 300;
		System.out.println(Cifras.mcd(a, b));
		System.out.println(Cifras.abs(-567));
	}

}
