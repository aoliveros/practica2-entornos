package source;

public class Letras {
	public static boolean esDecimal(String s) {
		return s.matches("^[0-9]+(\\.[0-9]+)?$");
	}
	public static int sumASCI(String s) {
		int aux=0;
		for(int i = 0; i< s.length(); i++) {
			aux += s.charAt(i);
		}
		return aux;
	}
	public static String toUpperCase(String s) {
		char aux[] = s.toCharArray();
		for(int i=0; i< aux.length; i++) {
			if(aux[i]>= 'a' || aux[i]<= 'z') {
				aux[i] -= ('a' - 'A');
			}
		}
		return String.copyValueOf(aux);
	}
	public static String toLowerCase(String s) {
		char aux[] = s.toCharArray();
		for(int i=0; i< aux.length; i++) {
			if(aux[i]>= 'A' || aux[i]<= 'Z') {
				aux[i] += ('a' - 'A');
			}
		}
		return String.copyValueOf(aux);
	}
	public static char startsWith(String s) {
		return s.charAt(0);
	}
}
