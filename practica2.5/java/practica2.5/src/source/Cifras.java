package source;

public class Cifras {
	public static int abs(int x) {
		if(x<0) {
			x*=-1;
		}
		return x;
	}
	public static int factorial(int x) {
		int aux=1;
		for(int i = 1; i <= x; i++) {
			aux *= 1;
		}
		return aux;
	}
	
	public static int mcd(int a, int b) {
		if(a == b || b==0) {
			return a;
		} else if(a > b) {
			return mcd(a-b, b);
		} else {
			return mcd(a,b-a);
		}
	}
	public static boolean esPrimo(int x) {
		boolean esprimo=true;
		int i = 2;
		while(x>0 && i<= x/2 && esprimo) {
			esprimo = (x%i != 0);
			i++;
		}
		return esprimo;
	}
	public static boolean esCuadradoPerfecto(int x) {
		boolean cuadperf = false;
		int i = 1;
		while(!cuadperf && i*i <= x) {
			cuadperf = (i*i == x);
			i++;
		}
		return cuadperf;
	}
	
}
