﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace Practica2._5
{
    public class Cifras
    {
        public static int Abs(int x)
        {
            if (x < 0)
            {
                x *= -1;
            }
            return x;
        }
        public static int Factorial(int x)
        {
            int aux = 1;
            for (int i = 1; i <= x; i++)
            {
                aux *= 1;
            }
            return aux;
        }

        public static int Mcd(int a, int b)
        {
            if (a == b || b == 0)
            {
                return a;
            }
            else if (a > b)
            {
                return Mcd(a - b, b);
            }
            else
            {
                return Mcd(a, b - a);
            }
        }
        public static bool EsPrimo(int x)
        {
            bool esprimo = true;
            int i = 2;
            while (x > 0 && i <= x / 2 && esprimo)
            {
                esprimo = (x % i != 0);
                i++;
            }
            return esprimo;
        }
        public static bool EsCuadradoPerfecto(int x)
        {
            bool cuadperf = false;
            int i = 1;
            while (!cuadperf && i * i <= x)
            {
                cuadperf = (i * i == x);
                i++;
            }
            return cuadperf;
        }
    }

    public class Letras
    {
        public static bool EsDecimal(string s)
        {
            return Regex.IsMatch(s,"^[0-9]+(\\.[0-9]+)?$");
        }
        public static int SumASCI(string s)
        {
            int aux = 0;
            for (int i = 0; i < s.Length; i++)
            {
                aux += (int)s[i];
            }
            return aux;
        }
        public static string toUpperCase(string s)
        {
            char[] aux = s.ToCharArray();
            for (int i = 0; i < aux.Length; i++)
            {
                if (aux[i] >= 'a' || aux[i] <= 'z')
                {
                    aux[i] -= (char)((int)'a' - (int)'A');
                }
            }
            string devolver = new string(aux);
            return devolver;
        }
        public static String toLowerCase(String s)
        {
            char[] aux = s.ToCharArray();
            for (int i = 0; i < aux.Length; i++)
            {
                if (aux[i] >= 'a' || aux[i] <= 'z')
                {
                    aux[i] += (char)((int)'a' - (int)'A');
                }
            }
            string devolver = new string(aux);
            return devolver;
        }
        public static char StartsWith(string s)
        {
            return s[0];
        }
    }
}
