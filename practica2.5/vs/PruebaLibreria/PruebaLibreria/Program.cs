﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PruebaLibreria
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine(Practica2._5.Cifras.Abs(-123));
            Console.WriteLine(Practica2._5.Cifras.Factorial(5));
            Console.WriteLine(Practica2._5.Cifras.Mcd(8000, 80));
            string cadena = "Hello World";
            Console.WriteLine(Practica2._5.Letras.toLowerCase(cadena));
            Console.WriteLine(Practica2._5.Letras.toUpperCase(cadena));
            Console.WriteLine(Practica2._5.Letras.StartsWith(cadena));
            Console.ReadKey();
        }
    }
}
